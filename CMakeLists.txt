#
# This is a template for a CMakeLists.txt file that can be used in a client
# project (work area) to set up building ATLAS packages against the configured
# release.
#

# Set the minimum required CMake version:
cmake_minimum_required( VERSION 3.7 FATAL_ERROR )
project( vertexdumper VERSION 1.0.0 )


# Bail out if no release is setup
if ( NOT DEFINED ENV{AtlasProject} )
      message(FATAL_ERROR "AtlasProject environment variable not set, can't figure out release")
      endif()

# Figure out what release we're in (e.g. x.y.z)
set ( AtlasVersion $ENV{$ENV{AtlasProject}_VERSION} )

# Set up that release
find_package( $ENV{AtlasProject} ${AtlasVersion} REQUIRED )

# Set up CTest:
atlas_ctest_setup()

# Set up a work directory project:
atlas_project( Dumper 1.0 USE $ENV{AtlasProject} ${AtlasVersion} )

# Set up the runtime environment setup script(s):
lcg_generate_env( SH_FILE ${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM}/env_setup.sh )
message(STATUS "Appending VertexDumper tab complete to setup")
install( FILES ${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM}/env_setup.sh
   DESTINATION . )

# Set up CPack:
atlas_cpack_setup()
