# Athena doesn't add h5ls to the path. This is a hacky way around that
# problem.
HDF5PATHS=( ${FASTJETPATH%/fastjet/*}/hdf5/*/${LCG_PLATFORM}/bin/ )
PATH+=:${HDF5PATHS}

