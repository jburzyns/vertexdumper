/*
  Copyright (C) 2002-2023 CERN for the benefit of the ATLAS collaboration
*/

#include "TrackDecoratorAlg.h"
#include "StoreGate/WriteDecorHandle.h"
#include "xAODEgamma/ElectronxAODHelpers.h"
#include <StoreGate/ReadDecorHandle.h>

namespace VertexDumper
{
  TrackDecoratorAlg ::TrackDecoratorAlg(const std::string &name,
                                        ISvcLocator *pSvcLocator)
      : AthReentrantAlgorithm(name, pSvcLocator)
  {
    declare(m_vertexWeight_key);
  }

  StatusCode TrackDecoratorAlg ::initialize()
  {
    ATH_CHECK(m_tracksInKey.initialize());

    ATH_CHECK(m_electronsInKey.initialize());
    ATH_CHECK(m_muonsInKey.initialize());
    ATH_CHECK(m_jetsInKey.initialize());
    ATH_CHECK(m_photonsInKey.initialize());

    // is this needed?
    m_vertexWeight_key = m_tracksInKey.key() + ".vertexWeight";
    ATH_CHECK(m_vertexWeight_key.initialize());

    m_ghostTrackKey = m_jetsInKey.key() + "." + m_ghostTrackKey.key();
    ATH_CHECK(m_ghostTrackKey.initialize());

    return StatusCode::SUCCESS;
  }

  StatusCode TrackDecoratorAlg ::execute(const EventContext &ctx) const
  {

    SG::ReadHandle<xAOD::TrackParticleContainer> tracksIn(m_tracksInKey, ctx);
    SG::ReadHandle<xAOD::JetContainer> jetsIn(m_jetsInKey, ctx);
    SG::ReadHandle<xAOD::ElectronContainer> electronsIn(m_electronsInKey, ctx);
    SG::ReadHandle<xAOD::MuonContainer> muonsIn(m_muonsInKey, ctx);
    SG::ReadHandle<xAOD::PhotonContainer> photonsIn(m_photonsInKey, ctx);

    ATH_CHECK(tracksIn.isValid());
    ATH_CHECK(electronsIn.isValid());
    ATH_CHECK(muonsIn.isValid());
    ATH_CHECK(photonsIn.isValid());
    ATH_CHECK(jetsIn.isValid());

    SG::AuxElement::Decorator<char> dec_isElectron("isElectron");
    SG::AuxElement::Decorator<char> dec_isMuon("isMuon");
    SG::AuxElement::Decorator<char> dec_isJet("isJet");

    SG::WriteDecorHandle<xAOD::TrackParticleContainer, float> vertexWeight(
        m_vertexWeight_key, ctx);

    for (const xAOD::TrackParticle *track : *tracksIn)
    {
      // is this needed?
      vertexWeight(*track) = 0.0; // initialize decorator to fill later on

      // is this track an electron track?
      bool isElectron = false;
      for (const xAOD::Electron *el : *electronsIn)
      {
        // The first track is the best-matched track
        const auto *gsf_trk = el->trackParticle(0);
        if (!gsf_trk)
          continue;
        const auto *id_trk =
            xAOD::EgammaHelpers::getOriginalTrackParticleFromGSF(gsf_trk);
        if (track == id_trk)
          isElectron = true;
      }
      dec_isElectron(*track) = isElectron;

      // is this track a muon track?
      bool isMuon = false;
      for (const xAOD::Muon *muon : *muonsIn)
      {
        const auto *trk =
            muon->trackParticle(xAOD::Muon::InnerDetectorTrackParticle);
        if (track == trk)
          isMuon = true;
      }
      dec_isMuon(*track) = isMuon;

      // is this track in a jet?
      bool isJet = false;
      using GhostList_t =
          std::vector<ElementLink<DataVector<xAOD::IParticle>>>;
      SG::ReadDecorHandle<xAOD::JetContainer, GhostList_t> ghostTrackAcc(
          m_ghostTrackKey, ctx);

      for (const xAOD::Jet *jet : *jetsIn)
      {
        const GhostList_t &ghosts = ghostTrackAcc(*jet);

        // now loop over the tracks in the jet
        for (const auto &trackLink : ghosts)
        {
          if (!trackLink.isValid())
            continue;
          const xAOD::TrackParticle *trk =
              dynamic_cast<const xAOD::TrackParticle *>(*trackLink);
          if (track == trk)
            isJet = true;
        }
      }
      dec_isJet(*track) = isJet;
    }
    return StatusCode::SUCCESS;
  }
} // namespace VertexDumper