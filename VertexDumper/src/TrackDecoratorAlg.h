/*
  Copyright (C) 2002-2022 CERN for the benefit of the ATLAS collaboration

  TrackDecoratorAlg:
  An alg that copies track information to aux decorations so can be
  output branch.
*/

// Always protect against multiple includes!
#ifndef EASYJET_TRACKDECORATORALG
#define EASYJET_TRACKDECORATORALG

#include <utility>
#include <vector>

#include <AthContainers/AuxElement.h>
#include <AthLinks/ElementLink.h>
#include <AthenaBaseComps/AthReentrantAlgorithm.h>
#include <StoreGate/WriteDecorHandleKey.h>
#include <xAODEgamma/ElectronContainer.h>
#include <xAODEgamma/PhotonContainer.h>
#include <xAODEventInfo/EventInfo.h>
#include <xAODJet/JetContainer.h>
#include <xAODMuon/MuonContainer.h>
#include <xAODTracking/TrackParticleContainer.h>

namespace VertexDumper
{

  /// \brief An algorithm for counting containers
  class TrackDecoratorAlg final : public AthReentrantAlgorithm
  {
    /// \brief The standard constructor
public:
    TrackDecoratorAlg(const std::string &name, ISvcLocator *pSvcLocator);

    /// \brief Initialisation method, for setting up tools and other persistent
    /// configs
    StatusCode initialize() override;
    /// \brief Execute method, for actions to be taken in the event loop
    StatusCode execute(const EventContext &ctx) const override;
    /// We use default finalize() -- this is for cleanup, and we don't do any

private:
    // Members for configurable properties
    SG::ReadHandleKey<xAOD::TrackParticleContainer> m_tracksInKey{
        this, "tracksIn", "InDetTrackParticles", "containerName to read"};
    SG::ReadHandleKey<xAOD::ElectronContainer> m_electronsInKey{
        this, "electronsIn", "Electrons", "containerName to read"};
    SG::ReadHandleKey<xAOD::MuonContainer> m_muonsInKey{
        this, "muonsIn", "Muons", "containerName to read"};
    SG::ReadHandleKey<xAOD::JetContainer> m_jetsInKey{
        this, "jetsIn", "AntiKt4EMTopoJets", "containerName to read"};
    SG::ReadHandleKey<xAOD::PhotonContainer> m_photonsInKey{
        this, "photonsIn", "Photons", "containerName to read"};

    SG::ReadDecorHandleKey<xAOD::JetContainer> m_ghostTrackKey{
        this, "ghostTrackKey", "GhostTrack", "Ghost collection to use"};

    // Internal members
    SG::WriteDecorHandleKey<xAOD::TrackParticleContainer> m_vertexWeight_key;
  };
} // namespace VertexDumper

#endif
