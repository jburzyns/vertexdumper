#include "../ElectronSelectorAlg.h"
#include "../JetSelectorAlg.h"
#include "../MuonSelectorAlg.h"
#include "../PhotonSelectorAlg.h"
#include "../TauDecoratorAlg.h"
#include "../TauSelectorAlg.h"
#include "../TrackDecoratorAlg.h"
#include "../VertexDecoratorAlg.h"

using namespace VertexDumper;

DECLARE_COMPONENT(JetSelectorAlg)
DECLARE_COMPONENT(PhotonSelectorAlg)
DECLARE_COMPONENT(MuonSelectorAlg)
DECLARE_COMPONENT(ElectronSelectorAlg)
DECLARE_COMPONENT(TauSelectorAlg)
DECLARE_COMPONENT(TauDecoratorAlg)
DECLARE_COMPONENT(TrackDecoratorAlg)
DECLARE_COMPONENT(VertexDecoratorAlg)
