/*
  Copyright (C) 2002-2022 CERN for the benefit of the ATLAS collaboration
*/

#ifndef HH4BANALYSIS_PHOTONSELECTORALG
#define HH4BANALYSIS_PHOTONSELECTORALG

#include <AthenaBaseComps/AthHistogramAlgorithm.h>
#include <SystematicsHandles/SysListHandle.h>
#include <SystematicsHandles/SysReadDecorHandle.h>
#include <SystematicsHandles/SysReadHandle.h>
#include <SystematicsHandles/SysWriteDecorHandle.h>
#include <SystematicsHandles/SysWriteHandle.h>

#include <AthContainers/ConstDataVector.h>
#include <xAODEgamma/PhotonContainer.h>
#include <xAODEventInfo/EventInfo.h>
#include <PhotonVertexSelection/PhotonPointingTool.h>

namespace VertexDumper
{

  /// \brief An algorithm for counting containers
  class PhotonSelectorAlg final : public AthHistogramAlgorithm
  {
    /// \brief The standard constructor
public:
    PhotonSelectorAlg(const std::string &name, ISvcLocator *pSvcLocator);

    /// \brief Initialisation method, for setting up tools and other persistent
    /// configs
    StatusCode initialize() override;
    /// \brief Execute method, for actions to be taken in the event loop
    StatusCode execute() override;
    /// We use default finalize() -- this is for cleanup, and we don't do any

private:
    // ToolHandle<whatever> handle {this, "pythonName", "defaultValue",
    // "someInfo"};

    /// \brief Setup syst-aware input container handles
    CP::SysListHandle m_systematicsList{this};

    CP::SysReadHandle<xAOD::EventInfo> m_eventHandle{
        this, "event", "EventInfo", "EventInfo container to read"};

    CP::SysReadHandle<xAOD::PhotonContainer> m_inHandle{
        this, "containerInKey", "", "Photon container to read"};

    // \brief Setup syst-aware input decorations
    CP::SysReadDecorHandle<char> m_isLoose{"DFCommonPhotonsIsEMLoose", this};

    /// \brief Setup syst-aware output container handles
    CP::SysWriteHandle<ConstDataVector<xAOD::PhotonContainer>> m_outHandle{
        this, "containerOutKey", "", "Photon container to write"};

    /// \brief Setup sys-aware output decorations
    CP::SysWriteDecorHandle<int> m_nSelPart{
        this, "decorOutName", "Photons_%SYS%",
        "Name out output decorator for number of selected photons"};

    /// \brief Setup photon pointing tool
    ToolHandle<CP::IPhotonPointingTool> m_pointingTool {this, "PhotonPointingTool","CP::PhotonPointingTool/PhotonVertexSelection",""};

    float m_minPt;
    float m_minEtaVeto;
    float m_maxEtaVeto;
    float m_maxEta;
    int m_minimumAmount;
    int m_truncateAtAmount;
    bool m_pTsort;
  };
} // namespace VertexDumper

#endif
