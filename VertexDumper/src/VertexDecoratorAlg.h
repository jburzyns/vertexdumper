/*
  Copyright (C) 2002-2022 CERN for the benefit of the ATLAS collaboration

  vertexDecoratorAlg:
  An alg that copies vertex information to aux decorations so can be
  output branch.
*/

// Always protect against multiple includes!
#ifndef EASYJET_VERTEXDECORATORALG
#define EASYJET_VERTEXDECORATORALG

#include <utility>
#include <vector>

#include <AthContainers/AuxElement.h>
#include <AthLinks/ElementLink.h>
#include <AthenaBaseComps/AthReentrantAlgorithm.h>
#include <InDetTruthVertexValidation/IInDetVertexTruthMatchTool.h>
#include <xAODEventInfo/EventInfo.h>
#include <xAODTracking/VertexContainer.h>


namespace VertexDumper
{

  /// \brief An algorithm for counting containers
  class VertexDecoratorAlg final : public AthReentrantAlgorithm
  {
    /// \brief The standard constructor
public:
    VertexDecoratorAlg(const std::string &name, ISvcLocator *pSvcLocator);

    /// \brief Initialisation method, for setting up tools and other persistent
    /// configs
    StatusCode initialize() override;
    /// \brief Execute method, for actions to be taken in the event loop
    StatusCode execute(const EventContext &ctx) const override;
    /// We use default finalize() -- this is for cleanup, and we don't do any

private:
    // Members for configurable properties
    SG::ReadHandleKey<xAOD::VertexContainer> m_vertexInKey{
        this, "vertexIn", "PrimaryVertices", "containerName to read"};
    SG::ReadHandleKey<xAOD::EventInfo> m_eventInKey{
        this, "eventIn", "EventInfo", "containerName to read"};

    // Internal members
    ToolHandle<IInDetVertexTruthMatchTool> m_vtxMatchTool{
        this, "VertexTruthMatchTool",
        "InDetVertexTruthMatchTool/VtxTruthMatchTool",
        "Vertex truth matching tool to use"};


  };
} // namespace VertexDumper

#endif
