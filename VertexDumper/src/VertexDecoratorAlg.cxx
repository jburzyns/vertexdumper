/*
  Copyright (C) 2002-2023 CERN for the benefit of the ATLAS collaboration
*/

#include "VertexDecoratorAlg.h"
#include "InDetTruthVertexValidation/InDetVertexTruthMatchTool.h"
#include "InDetTruthVertexValidation/InDetVertexTruthMatchUtils.h"
#include "xAODTracking/TrackParticle.h"
#include "AthContainers/ConstDataVector.h"
#include "xAODTruth/xAODTruthHelpers.h"

namespace VertexDumper
{
  VertexDecoratorAlg ::VertexDecoratorAlg(const std::string &name,
                                          ISvcLocator *pSvcLocator)
      : AthReentrantAlgorithm(name, pSvcLocator)
  {
  }

  StatusCode VertexDecoratorAlg ::initialize()
  {
    ATH_CHECK(m_vertexInKey.initialize());
    ATH_CHECK(m_eventInKey.initialize());
    ATH_CHECK(m_vtxMatchTool.retrieve());

    return StatusCode::SUCCESS;
  }

StatusCode VertexDecoratorAlg::execute(const EventContext &ctx) const
{
  SG::ReadHandle<xAOD::VertexContainer> vertices(m_vertexInKey, ctx);
  ATH_CHECK(vertices.isValid());
  SG::ReadHandle<xAOD::EventInfo> eventInfo(m_eventInKey, ctx);
  ATH_CHECK(eventInfo.isValid());


  // Decorate vertices
  if (m_vtxMatchTool)
  {
    ATH_CHECK(m_vtxMatchTool->matchVertices(*vertices));
    ATH_MSG_DEBUG(
        "Hard scatter classification type: "
        << InDetVertexTruthMatchUtils::classifyHardScatter(*vertices)
        << ", vertex container size = " << vertices->size());
  }

  const xAOD::Vertex *bestRecoHSVtx_truth =
      InDetVertexTruthMatchUtils::bestHardScatterMatch(*vertices);

  SG::AuxElement::Decorator<int> dec_label("isHardScatter");
  SG::AuxElement::Decorator<unsigned long> dec_eventNumber("eventNumber");
  for (const xAOD::Vertex *vertex : *vertices)
  {
    if (vertex->vertexType() == xAOD::VxType::NoVtx)
      continue;

    // for classification label
    dec_label(*vertex) = (vertex == bestRecoHSVtx_truth);

    // for test/train/val split
    dec_eventNumber(*vertex) = eventInfo->eventNumber();

  }

  return StatusCode::SUCCESS;
}
}