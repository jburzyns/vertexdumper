# Copyright (C) 2002-2023 CERN for the benefit of the ATLAS collaboration

from AthenaCommon import Logging

log = Logging.logging.getLogger("run-ntupler")

_rogue_loggers = [
    "Athena",
    "AutoConfigFlags",
    "MetaReader",
    "makePileupAnalysisSequence",
    "ConfigurableDb",
]


def setRogueLoggers(level):
    for x in _rogue_loggers:
        Logging.logging.getLogger(x).setLevel(level)
