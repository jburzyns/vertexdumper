# Copyright (C) 2002-2023 CERN for the benefit of the ATLAS collaboration

import pathlib
from argparse import ArgumentParser, Namespace

from AthenaConfiguration.AthConfigFlags import AthConfigFlags


def add_standard_athena_args(parser: ArgumentParser) -> None:
    """Custom version of the arguments flags

    The names of the arguments are stolen from Athena, see here:

    https://gitlab.cern.ch/atlas/athena/-/blob/release/22.2.110/Control/AthenaConfiguration/python/AthConfigFlags.py#L434

    Since this was introduced we also stole the parser function that
    fills the configuration flags from the arguments, see below.

    """
    parser.add_argument(
        "filesInput",
        metavar="input_files",
        help="Input file(s) as a comma separated list, supports * wildcard",
    )
    parser.add_argument(
        "-d",
        "--debug",
        nargs="?",
        const="exec",
        choices=["init", "exec", "fini"],
        metavar="STAGE",
        default=None,
        help="attach debugger at stage, default to '%(const)s'",
    )
    parser.add_argument(
        "-e", "--evtMax", type=int, default=None, help="Max number of events to process"
    )
    parser.add_argument(
        "-k",
        "--skipEvents",
        metavar="N",
        type=int,
        default=None,
        help="Number of events to skip",
    )
    parser.add_argument(
        "-l",
        "--loglevel",
        nargs="?",
        default="INFO",
        const="INFO",
        choices=["ALL", "VERBOSE", "DEBUG", "INFO", "WARNING", "ERROR", "FATAL"],
        metavar="LEVEL",
        help="logging level",
    )


def fill_from_args(flags: AthConfigFlags, parser: ArgumentParser) -> Namespace:
    """
    Copied (and simplified) from athena's over-featured version
    """

    args = parser.parse_args()

    if args.debug is not None:
        from AthenaCommon.Debugging import DbgStage

        if args.debug not in DbgStage.allowed_values:
            raise ValueError(
                "Unknown debug stage, allowed values {}".format(DbgStage.allowed_values)
            )
        flags.Exec.DebugStage = args.debug

    if args.evtMax is not None:
        flags.Exec.MaxEvents = args.evtMax

    if args.skipEvents is not None:
        flags.Exec.SkipEvents = args.skipEvents

    flags.Input.Files = []  # remove generic
    for ffile in args.filesInput.split(","):
        if "*" in ffile:  # handle wildcard
            import glob

            flags.Input.Files += glob.glob(ffile)
        else:
            flags.Input.Files += [ffile]

    if args.loglevel is not None:
        from AthenaCommon import Constants

        if hasattr(Constants, args.loglevel):
            flags.Exec.OutputLevel = getattr(Constants, args.loglevel)
        else:
            raise ValueError(
                "Unknown log-level, allowed values are"
                " ALL, VERBOSE, DEBUG,INFO, WARNING, ERROR, FATAL"
            )

    flags.addFlag("Analysis.out_file", args.out_file)
    flags.addFlag("Analysis.ntracks", args.ntracks)
    flags.addFlag("Analysis.nmuons", args.nmuons)
    flags.addFlag("Analysis.nelectrons", args.nelectrons)
    flags.addFlag("Analysis.njets", args.njets)
    flags.addFlag("Analysis.nphotons", args.nphotons)

    return args


class AnalysisArgumentParser(ArgumentParser):
    def __init__(self):
        super().__init__()
        # Generate a parser and add an output file argument, then retrieve the args
        add_standard_athena_args(self)

        self.add_argument(
            "-o",
            "--out-file",
            metavar="H5OUT",
            type=pathlib.Path,
            default=False,
            help="save output HDF5 file",
        )
        self.add_argument(
            "-n",
            "--ntracks",
            metavar="NTRK",
            type=int,
            default=300,
            help="number of tracks to save per vertex",
        )
        self.add_argument(
            "-mu",
            "--nmuons",
            metavar="NMU",
            type=int,
            default=20,
            help="number of muons to save per vertex",
        )
        self.add_argument(
            "-el",
            "--nelectrons",
            metavar="NEL",
            type=int,
            default=20,
            help="number of electrons to save per vertex",
        )
        self.add_argument(
            "-j",
            "--njets",
            metavar="NJET",
            type=int,
            default=20,
            help="number of jets to save per vertex",
        )
        self.add_argument(
            "-p",
            "--nphotons",
            metavar="NPH",
            type=int,
            default=20,
            help="number of photons to save per vertex",
        )