# Copyright (C) 2002-2023 CERN for the benefit of the ATLAS collaboration

from AthenaConfiguration.ComponentAccumulator import ComponentAccumulator
from AthenaConfiguration.ComponentFactory import CompFactory
from AthenaConfiguration.MainServicesConfig import MainServicesCfg
from EventBookkeeperTools.EventBookkeeperToolsConfig import (
    BookkeeperToolCfg,
    CutFlowSvcCfg,
)
from PerfMonComps.PerfMonCompsConfig import PerfMonMTSvcCfg
from VertexDumper.output_config import get_h5_cfg

# Set up the basic event reading infrastructure
# Retrieve this as the starting CA to merge others into
# This should not be merged into a bare CA, as this
# can mess up the app configuration
def core_services_cfg(flags):
    # Get a ComponentAccumulator setting up the standard components
    # needed to run an Athena job.
    cfg = MainServicesCfg(flags)

    if flags.PerfMon.doFullMonMT:
        cfg.merge(PerfMonMTSvcCfg(flags))

    # Avoid stack traces to the exception handler. These traces
    # aren't very useful since they just point to the handler, not
    # the original bug.
    cfg.addService(CompFactory.ExceptionSvc(Catch="NONE"))

    # Needed for filtering, Athena only for now
    # Create CutFlowSvc otherwise the default CutFlowSvc that has only
    # one CutflowBookkeeper object, and can't deal with multiple weights
    cfg.merge(CutFlowSvcCfg(flags))
    cfg.merge(BookkeeperToolCfg(flags))
    # Adjust the loop manager to announce the event number less frequently.
    # Makes a big difference if running over many events
    if flags.Concurrency.NumThreads > 0:
        cfg.addService(CompFactory.AthenaHiveEventLoopMgr(EventPrintoutInterval=1))
    else:
        cfg.addService(CompFactory.AthenaEventLoopMgr(EventPrintoutInterval=1))

    from AthenaPoolCnvSvc.PoolReadConfig import PoolReadCfg

    cfg.merge(PoolReadCfg(flags))

    return cfg

# Analysis algorithms
def analysis_cfg(flags, seqname):
    cfg = ComponentAccumulator()

    # build the jets
    from JetRecConfig.JetRecConfig import JetRecCfg
    from JetRecConfig.StandardSmallRJets import AntiKt4EMTopo

    cfg.merge(JetRecCfg(flags, AntiKt4EMTopo))

    sysSvc = CompFactory.CP.SystematicsSvc("SystematicsSvc")
    cfg.addService(sysSvc)



    inputCollections = {
        "jets": "AntiKt4EMTopoJets",
        "electrons": "Electrons",
        "muons": "Muons",
        "photons": "Photons",
    }


    cfg.addEventAlgo(
        CompFactory.VertexDumper.TrackDecoratorAlg(
            "Decor_InDetTrackParticles",
            tracksIn="InDetTrackParticles",
            electronsIn="Electrons",
            muonsIn="Muons",
            photonsIn="Photons",
            jetsIn="AntiKt4EMTopoJets",
        )
    )


    cfg.addEventAlgo(
        CompFactory.VertexDumper.VertexDecoratorAlg(
            "Decor_PrimaryVertices",
            vertexIn="PrimaryVertices",
        )
    )



    from InDetConfig.InDetGNNHardScatterSelectionConfig import (
        GNNSequenceCfg)
    cfg.merge(GNNSequenceCfg(flags))

    return cfg





# Configure output file writing
def output_cfg(flags, seqname):
    cfg = ComponentAccumulator()
    cfg.addSequence(CompFactory.AthSequencer(seqname), "AthAlgSeq")

    cfg.merge(
        get_h5_cfg(flags),
        seqname,
    )

    return cfg
