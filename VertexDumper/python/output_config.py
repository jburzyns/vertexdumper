# Copyright (C) 2002-2023 CERN for the benefit of the ATLAS collaboration

from AthenaConfiguration.ComponentAccumulator import ComponentAccumulator
from AthenaConfiguration.ComponentFactory import CompFactory

def get_h5_cfg(flags):
    ca = ComponentAccumulator()
    output = CompFactory.H5FileSvc(path=flags.Analysis.out_file.as_posix())
    ca.addService(output)

    ca.addEventAlgo(
        CompFactory.VertexWriterAlg(
            "vertexwriter",
            primitives=[
                "ntrk",
                "sumPt2",
                "sumPt",
                "chi2Over_ndf",
                "z_asymmetry",
                "weighted_z_asymmetry",
                "z_kurtosis",
                "z_skewness",
                "photon_deltaz",
                "photon_deltaPhi",
                "isHardScatter",
                "eventNumber",
                "actualIntPerXing",
                "HSGN2_phsvertex"
            ],
            primitiveToType={
                "ntrk": "INT",
                "sumPt2": "FLOAT",
                "sumPt": "FLOAT",
                "chi2Over_ndf": "FLOAT",
                "z_asymmetry": "FLOAT",
                "weighted_z_asymmetry": "FLOAT",
                "z_kurtosis": "FLOAT",
                "z_skewness": "FLOAT",
                "photon_deltaz": "FLOAT",
                "photon_deltaPhi": "FLOAT",
                "isHardScatter": "INT",
                "eventNumber": "UL2ULL",
                "actualIntPerXing": "FLOAT",
                "HSGN2_phsvertex" : "FLOAT",

            },
            datasetName="vertices",
            output=output,
        )
    )

    ca.addEventAlgo(
        CompFactory.IParticleWriterAlg(
            "trackwriter",
            primitives=[
                "valid",
                "pt",
                "eta",
                "phi",
                "vertexWeight",
                "isElectron",
                "isMuon",
                "isJet",
            ],
            primitiveToType={
                "valid": "CUSTOM",
                "pt": "CUSTOM",
                "eta": "CUSTOM",
                "phi": "CUSTOM",
                "vertexWeight": "FLOAT",
                "isElectron": "CHAR",
                "isMuon": "CHAR",
                "isJet": "CHAR",
            },
            primitiveToAssociation={},
            datasetName="tracks",
            maximumSize=flags.Analysis.ntracks,
            output=output,
        )    
    )

    ca.addEventAlgo(
        CompFactory.MuonWriterAlg(
            "muonwriter",
            primitives=[
                "valid",
                "pt",
                "eta",
                "phi",
                "deltaZ0",
            ],
            primitiveToType={
                "valid": "CUSTOM",
                "pt": "CUSTOM",
                "eta": "CUSTOM",
                "phi": "CUSTOM",
                "deltaZ0" : "FLOAT",
            },
            primitiveToAssociation={},
            datasetName="muons",
            maximumSize=flags.Analysis.nmuons,
            # muonContainer="Muons_OR",
            output=output,
        )    
    )

    ca.addEventAlgo(
        CompFactory.ElectronWriterAlg(
            "electronwriter",
            primitives=[
                "valid",
                "pt",
                "eta",
                "phi",
                "deltaZ0",
            ],
            primitiveToType={
                "valid": "CUSTOM",
                "pt": "CUSTOM",
                "eta": "CUSTOM",
                "phi": "CUSTOM",
                "deltaZ0" : "FLOAT",
            },
            primitiveToAssociation={},
            datasetName="electrons",
            maximumSize=flags.Analysis.nelectrons,
            output=output,
        )   
    )
    
    ca.addEventAlgo(
        CompFactory.JetWriterAlg(
            "jetwriter",
            primitives=[
                "valid",
                "pt",
                "eta",
                "phi",
                "ntracks_ga",
            ],
            primitiveToType={
                "valid": "CUSTOM",
                "pt": "CUSTOM",
                "eta": "CUSTOM",
                "phi": "CUSTOM",
                "ntracks_ga": "FLOAT",
            },
            primitiveToAssociation={},
            datasetName="jets",
            maximumSize=flags.Analysis.njets,
            jetContainer="AntiKt4EMTopoJets_OR",
            output=output,
        )   
    )
    
    ca.addEventAlgo(
        CompFactory.PhotonWriterAlg(
            "photonwriter",
            primitives=[
                "valid",
                "pt",
                "eta",
                "phi",
                # "photonIsConverted",
                "photon_deltaZ",
                # "photon_deltaZsigma",
                # "photon_deltaZsigni",
                "photon_deltaZ_wBeamSpot"
            ],
            primitiveToType={
                "valid": "CUSTOM",
                "pt": "CUSTOM",
                "eta": "CUSTOM",
                "phi": "CUSTOM",
                # "photonIsConverted": "CHAR",
                "photon_deltaZ" : "FLOAT",
                # "photon_deltaZsigma" : "FLOAT",
                # "photon_deltaZsigni" : "FLOAT",
                "photon_deltaZ_wBeamSpot" : "FLOAT",
            },
            primitiveToAssociation={},
            datasetName="photons",
            maximumSize=flags.Analysis.nphotons,
            output=output,
        )   
    )
    
    return ca
