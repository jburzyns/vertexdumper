# VertexDumper framework

This is a framework to prepare training samples for an exploratory new hard-scatter selection
algorithm.

## Setting up

First, clone the repository in a new working directory

```
git clone ssh://git@gitlab.cern.ch:7999/jburzyns/vertexdumper.git
```
Now, compile the package

```
mkdir build
cd build
source ../vertexdumper/setup.sh
cmake ../vertexdumper/
make
source */setup.sh
```

## Running

After compiling the package, the `run-ntupler` executable will be added to your `PATH`.
To run over a file, use the following command:
```
run-ntupler myinputfile.AOD.pool.root --evtMax 10 --out-file output.h5
```

### Grid submissions

An executable is provided for running on the grid:
```
grid-submit --input-list datasets.txt
```
where `datasets.txt` is a text file containing a list of AOD datasets that you wish to run over.
