# setup ATLAS
export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase

echo "=== running setupATLAS ==="
source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh -q

echo "=== running asetup ==="
asetup Athena,main,latest

# add h5ls
echo "=== adding hdf5 tools ==="
SCRIPT_PATH=${BASH_SOURCE[0]:-${0}}
if [[ $SCRIPT_PATH =~ / ]] ; then
    source ${SCRIPT_PATH%/*}/add_h5_tools.sh
else
    source add_h5_tools.sh
fi
echo "=== done ==="