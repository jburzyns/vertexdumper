#ifndef EVENT_INFO_WRITER
#define EVENT_INFO_WRITER

#include <memory>

namespace H5
{
  class Group;
}
namespace H5Utils
{
  template <size_t N, typename I> class Writer;
}
namespace xAOD
{
  class Vertex_v1;
  typedef Vertex_v1 Vertex;
} // namespace xAOD
struct VertexWriterConfig;

class VertexWriter
{
  public:
  using Writer_t = H5Utils::Writer<0, const xAOD::Vertex &>;
  VertexWriter(H5::Group &output_group, const VertexWriterConfig &);
  ~VertexWriter();
  void fill(const xAOD::Vertex &);
  void flush();

  private:
  std::unique_ptr<Writer_t> m_writer;
};

#endif
