#ifndef JET_WRITER_H
#define JET_WRITER_H

#include <memory>
#include <vector>
#include <xAODJet/Jet.h>

namespace H5
{
  class Group;
}
namespace H5Utils
{
  template <size_t N, typename I> class Writer;
}
struct JetWriterConfig;

namespace details
{
  // implementation depends on type of array we're storing
  class JetWriterBase;
} // namespace details

class JetWriter
{
  public:
  JetWriter(H5::Group &output_group, const JetWriterConfig &);
  ~JetWriter();
  void fill(const std::vector<const xAOD::Jet *> &);
  void flush();

  private:
  std::unique_ptr<details::JetWriterBase> m_writer;
};

#endif
