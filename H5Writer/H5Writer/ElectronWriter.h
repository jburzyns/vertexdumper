#ifndef ELECTRON_WRITER_H
#define ELECTRON_WRITER_H

#include <memory>
#include <vector>
#include <xAODEgamma/Electron.h>

namespace H5
{
  class Group;
}
namespace H5Utils
{
  template <size_t N, typename I> class Writer;
}
struct ElectronWriterConfig;

namespace details
{
  // implementation depends on type of array we're storing
  class ElectronWriterBase;
} // namespace details

class ElectronWriter
{
  public:
  ElectronWriter(H5::Group &output_group, const ElectronWriterConfig &);
  ~ElectronWriter();
  void fill(const std::vector<const xAOD::Electron *> &);
  void flush();

  private:
  std::unique_ptr<details::ElectronWriterBase> m_writer;
};

#endif
