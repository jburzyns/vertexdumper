#ifndef MUON_WRITER_H
#define MUON_WRITER_H

#include <memory>
#include <vector>
#include <xAODMuon/Muon.h>

namespace H5
{
  class Group;
}
namespace H5Utils
{
  template <size_t N, typename I> class Writer;
}
struct MuonWriterConfig;

namespace details
{
  // implementation depends on type of array we're storing
  class MuonWriterBase;
} // namespace details

class MuonWriter
{
  public:
  MuonWriter(H5::Group &output_group, const MuonWriterConfig &);
  ~MuonWriter();
  void fill(const std::vector<const xAOD::Muon *> &);
  void flush();

  private:
  std::unique_ptr<details::MuonWriterBase> m_writer;
};

#endif
