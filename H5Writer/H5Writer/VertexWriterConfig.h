#ifndef EVENT_INFO_WRITER_CONFIG
#define EVENT_INFO_WRITER_CONFIG

#include "Primitive.h"

#include <string>
#include <vector>

struct VertexWriterConfig
{
  std::string name;
  std::vector<Primitive> inputs;
};

#endif
