#ifndef PHOTON_WRITER_H
#define PHOTON_WRITER_H

#include <memory>
#include <vector>
#include <xAODEgamma/Photon.h>

namespace H5
{
  class Group;
}
namespace H5Utils
{
  template <size_t N, typename I> class Writer;
}
struct PhotonWriterConfig;

namespace details
{
  // implementation depends on type of array we're storing
  class PhotonWriterBase;
} // namespace details

class PhotonWriter
{
  public:
  PhotonWriter(H5::Group &output_group, const PhotonWriterConfig &);
  ~PhotonWriter();
  void fill(const std::vector<const xAOD::Photon *> &);
  void flush();

  private:
  std::unique_ptr<details::PhotonWriterBase> m_writer;
};

#endif
