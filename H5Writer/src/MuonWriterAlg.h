#ifndef MUON_WRITER_ALG_H
#define MUON_WRITER_ALG_H

#include "H5Writer/IH5GroupSvc.h"
#include "H5Writer/MuonWriter.h"

#include "AthenaBaseComps/AthAlgorithm.h"
#include "GaudiKernel/ServiceHandle.h"
#include "xAODTracking/VertexContainer.h"
#include "xAODMuon/MuonContainer.h"

class MuonWriterAlg : public AthAlgorithm
{
  public:
  MuonWriterAlg(const std::string &name, ISvcLocator *loc);

  virtual StatusCode initialize() override;
  virtual StatusCode execute() override;
  virtual StatusCode finalize() override;

  private:
  Gaudi::Property<std::vector<std::string>> m_primitives{
      this, "primitives", {}, "List of primatives to print"};
  Gaudi::Property<std::map<std::string, std::string>> m_primToType{
      this, "primitiveToType", {}, "Map from primitive to type"};
  Gaudi::Property<std::map<std::string, std::string>> m_primToAssociation{
      this, "primitiveToAssociation", {}, "Map from primitive to association"};
  Gaudi::Property<std::string> m_dsName{this, "datasetName", "",
                                        "Name of output dataset"};
  Gaudi::Property<unsigned long long> m_maxSize{
      this, "maximumSize", 10, "Maximum number of particles to store"};
  SG::ReadHandleKey<xAOD::VertexContainer> m_vertexKey{
      this, "vertexContainer", "PrimaryVertices", "Vertex container key"};
   SG::ReadDecorHandleKey<xAOD::VertexContainer> m_muonLinksKey{
      this, "muonLinks", "muonLinks", "" };

  ServiceHandle<IH5GroupSvc> m_output_svc{this, "output", "",
                                          "output file service"};

  std::unique_ptr<MuonWriter> m_writer;
};

#endif
