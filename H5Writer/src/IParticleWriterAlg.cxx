#include "src/IParticleWriterAlg.h"
#include "H5Writer/IParticleWriterConfig.h"
#include "xAODTracking/TrackParticle.h"

#include "AlgHelpers.h"

#include "H5Cpp.h"

IParticleWriterAlg::IParticleWriterAlg(const std::string &name,
                                       ISvcLocator *loc)
    : AthAlgorithm(name, loc), m_writer(nullptr)
{
}

StatusCode IParticleWriterAlg::initialize()
{
  ATH_CHECK(m_vertexKey.initialize());
  ATH_CHECK(m_output_svc.retrieve());

  IParticleWriterConfig cfg;
  cfg.name = m_dsName.value();
  cfg.maximum_size = m_maxSize.value();
  if (cfg.name.empty())
  {
    ATH_MSG_ERROR("datasetName isn't specified in particle writer");
    return StatusCode::FAILURE;
  }
  for (const std::string &prim : m_primitives)
  {
    if (!m_primToType.value().count(prim))
    {
      ATH_MSG_ERROR(prim << " not specified in type mapping");
      return StatusCode::FAILURE;
    }
    auto type = getPrimitiveType(m_primToType.value().at(prim));
    if (m_primToAssociation.value().count(prim))
    {
      std::string path = m_primToAssociation.value().at(prim);
      size_t pos = path.find('/');
      if (pos == std::string::npos)
      {
        ATH_MSG_ERROR("no '/' in " << path);
        return StatusCode::FAILURE;
      }
      std::string link_name = path.substr(0, pos);
      std::string source_name = path.substr(pos + 1);
      Primitive newprim{type, source_name, prim};
      cfg.inputs.push_back(AssociatedPrimitive{link_name, newprim});
    }
    else
    {
      Primitive newprim{type, prim, prim};
      cfg.inputs.push_back(AssociatedPrimitive{"", newprim});
    }
  }
  m_writer.reset(new IParticleWriter(*m_output_svc->group(), cfg));

  return StatusCode::SUCCESS;
}

StatusCode IParticleWriterAlg::execute()
{

  SG::ReadHandle<xAOD::VertexContainer> vertexContainer(m_vertexKey);
  ATH_CHECK(vertexContainer.isValid());



  for (const xAOD::Vertex *vertex : *vertexContainer)
  {
    if (vertex->vertexType() == xAOD::VxType::NoVtx)
      continue;


    std::vector<std::pair<int,float> > tracks_index_pt;
    std::vector<const xAOD::IParticle *> parts;
    std::vector<const xAOD::IParticle *> selected_parts;
    SG::AuxElement::Decorator<float> deco("vertexWeight");
    SG::AuxElement::Accessor<float> acc("vertexWeight");
    for (size_t i = 0; i < vertex->nTrackParticles(); i++)
    {
      xAOD::TrackParticle* storeTrack = new xAOD::TrackParticle();
      *storeTrack = *vertex->trackParticle(i); 
      deco(*storeTrack) = vertex->trackWeight(i);
      parts.push_back(storeTrack);
      tracks_index_pt.push_back(std::make_pair(storeTrack->pt(),i));
    }

    //Sort the tracks based on pT
    sort(tracks_index_pt.begin(),tracks_index_pt.end(), std::greater<>());
    for(long unsigned int i = 0; i < parts.size(); i++){
      selected_parts.push_back(parts.at(tracks_index_pt.at(i).second));
    }

    m_writer->fill(selected_parts);

    for(auto part : parts) delete part;

  }
  return StatusCode::SUCCESS;
}

StatusCode IParticleWriterAlg::finalize()
{
  m_writer->flush();
  return StatusCode::SUCCESS;
}
