#ifndef JET_WRITER_ALG_H
#define JET_WRITER_ALG_H

#include "H5Writer/IH5GroupSvc.h"
#include "H5Writer/JetWriter.h"

#include "AthenaBaseComps/AthAlgorithm.h"
#include "GaudiKernel/ServiceHandle.h"
#include "xAODTracking/VertexContainer.h"
#include "xAODJet/JetContainer.h"

#include "TrackVertexAssociationTool/TrackVertexAssociationTool.h"

class JetWriterAlg : public AthAlgorithm
{
  public:
  JetWriterAlg(const std::string &name, ISvcLocator *loc);

  virtual StatusCode initialize() override;
  virtual StatusCode execute() override;
  virtual StatusCode finalize() override;

  private:
  Gaudi::Property<std::vector<std::string>> m_primitives{
      this, "primitives", {}, "List of primatives to print"};
  Gaudi::Property<std::map<std::string, std::string>> m_primToType{
      this, "primitiveToType", {}, "Map from primitive to type"};
  Gaudi::Property<std::map<std::string, std::string>> m_primToAssociation{
      this, "primitiveToAssociation", {}, "Map from primitive to association"};
  Gaudi::Property<std::string> m_dsName{this, "datasetName", "",
                                        "Name of output dataset"};
  Gaudi::Property<unsigned long long> m_maxSize{
      this, "maximumSize", 10, "Maximum number of particles to store"};
  SG::ReadHandleKey<xAOD::VertexContainer> m_vertexKey{
      this, "vertexContainer", "PrimaryVertices", "Vertex container key"};
  SG::ReadHandleKey<xAOD::JetContainer> m_jetKey{
      this, "jetContainer", "Jets", "Jet container key"};
   SG::ReadDecorHandleKey<xAOD::VertexContainer> m_jetLinksKey{
      this, "jetLinks", "jetLinks", "" };

  ServiceHandle<IH5GroupSvc> m_output_svc{this, "output", "",
                                          "output file service"};

  ToolHandle<CP::TrackVertexAssociationTool> m_trkVtxAssociationTool{this, "TrackVertexAssociationTool", "CP::TrackVertexAssociationTool/TrackVertexAssociationTool", "Track vertex association tool to use"};

  std::unique_ptr<JetWriter> m_writer;
};

#endif
