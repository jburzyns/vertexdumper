#include "src/H5FileSvc.h"
#include "src/IParticleWriterAlg.h"
#include "src/VertexWriterAlg.h"
#include "src/MuonWriterAlg.h"
#include "src/ElectronWriterAlg.h"
#include "src/JetWriterAlg.h"
#include "src/PhotonWriterAlg.h"

DECLARE_COMPONENT(H5FileSvc)
DECLARE_COMPONENT(VertexWriterAlg)
DECLARE_COMPONENT(IParticleWriterAlg)
DECLARE_COMPONENT(MuonWriterAlg)
DECLARE_COMPONENT(ElectronWriterAlg)
DECLARE_COMPONENT(JetWriterAlg)
DECLARE_COMPONENT(PhotonWriterAlg)