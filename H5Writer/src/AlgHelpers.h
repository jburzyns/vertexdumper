#ifndef ALG_HELPERS_H
#define ALG_HELPERS_H

#include "H5Writer/Primitive.h"

#include <string>

Primitive::Type getPrimitiveType(const std::string &name);

#endif
