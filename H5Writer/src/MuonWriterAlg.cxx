#include "src/MuonWriterAlg.h"
#include "H5Writer/MuonWriterConfig.h"
#include "xAODTracking/TrackParticle.h"
#include "xAODTruth/xAODTruthHelpers.h"
#include "xAODTruth/TruthParticle.h"
#include "AsgDataHandles/ReadDecorHandle.h"

#include "AlgHelpers.h"

#include "H5Cpp.h"

MuonWriterAlg::MuonWriterAlg(const std::string &name,
                                       ISvcLocator *loc)
    : AthAlgorithm(name, loc), m_writer(nullptr)
{
}

StatusCode MuonWriterAlg::initialize()
{
  ATH_CHECK(m_vertexKey.initialize());

  ATH_CHECK(m_muonLinksKey.initialize());

  ATH_CHECK(m_output_svc.retrieve());

  MuonWriterConfig cfg;
  cfg.name = m_dsName.value();
  cfg.maximum_size = m_maxSize.value();
  if (cfg.name.empty())
  {
    ATH_MSG_ERROR("datasetName isn't specified in particle writer");
    return StatusCode::FAILURE;
  }
  for (const std::string &prim : m_primitives)
  {
    if (!m_primToType.value().count(prim))
    {
      ATH_MSG_ERROR(prim << " not specified in type mapping");
      return StatusCode::FAILURE;
    }
    auto type = getPrimitiveType(m_primToType.value().at(prim));
    if (m_primToAssociation.value().count(prim))
    {
      std::string path = m_primToAssociation.value().at(prim);
      size_t pos = path.find('/');
      if (pos == std::string::npos)
      {
        ATH_MSG_ERROR("no '/' in " << path);
        return StatusCode::FAILURE;
      }
      std::string link_name = path.substr(0, pos);
      std::string source_name = path.substr(pos + 1);
      Primitive newprim{type, source_name, prim};
      cfg.inputs.push_back(AssociatedPrimitive{link_name, newprim});
    }
    else
    {
      Primitive newprim{type, prim, prim};
      cfg.inputs.push_back(AssociatedPrimitive{"", newprim});
    }
  }
  m_writer.reset(new MuonWriter(*m_output_svc->group(), cfg));

  return StatusCode::SUCCESS;
}

StatusCode MuonWriterAlg::execute()
{
  const EventContext &ctx = Gaudi::Hive::currentContext();

  SG::ReadHandle<xAOD::VertexContainer> vertexContainer(m_vertexKey);
  ATH_CHECK(vertexContainer.isValid());


  const std::string baseName = m_vertexKey.key();
  m_muonLinksKey = baseName + ".muonLinks";
  SG::ReadDecorHandle<xAOD::VertexContainer, std::vector<ElementLink<xAOD::MuonContainer>>> acc_muonLinks(m_muonLinksKey, ctx);
  ATH_CHECK(acc_muonLinks.isValid());



  //This to modify with the muons to store
  for (const xAOD::Vertex *vertex : *vertexContainer)
  {
    if (vertex->vertexType() == xAOD::VxType::NoVtx)
      continue;


    const std::vector<ElementLink<xAOD::MuonContainer>>& muonLinks = acc_muonLinks(*vertex);

    std::vector<const xAOD::Muon *> parts;
    SG::AuxElement::Decorator<float> deco("deltaZ0");
    SG::AuxElement::Accessor<float> acc("deltaZ0");

    for (const auto& link : muonLinks) {
      if (!link.isValid()) continue;
      const xAOD::Muon* muon = *link;
      if (!muon) continue;

      auto tp = muon->trackParticle(xAOD::Muon::InnerDetectorTrackParticle);
      if(tp){
        deco(*muon) = tp->z0() + tp->vz() - vertex->z(); 
      }else{
        deco(*muon) = -999;
      }
      parts.push_back(muon);
    }
    m_writer->fill(parts);
    parts.clear();
  }
  return StatusCode::SUCCESS;
}

StatusCode MuonWriterAlg::finalize()
{
  m_writer->flush();
  return StatusCode::SUCCESS;
}
