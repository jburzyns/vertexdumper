#include "src/PhotonWriterAlg.h"
#include "H5Writer/PhotonWriterConfig.h"
#include "xAODTracking/TrackParticle.h"
#include "xAODTruth/xAODTruthHelpers.h"
#include "xAODTruth/TruthParticle.h"
#include "PhotonVertexSelection/PhotonVertexHelpers.h"
#include "AsgDataHandles/ReadDecorHandle.h"

#include "AlgHelpers.h"

#include "H5Cpp.h"

PhotonWriterAlg::PhotonWriterAlg(const std::string &name,
                                       ISvcLocator *loc)
    : AthAlgorithm(name, loc), m_writer(nullptr)
{
}

StatusCode PhotonWriterAlg::initialize()
{
  ATH_CHECK(m_vertexKey.initialize());
  ATH_CHECK(m_photonLinksKey.initialize());
  ATH_CHECK(m_output_svc.retrieve());

  PhotonWriterConfig cfg;
  cfg.name = m_dsName.value();
  cfg.maximum_size = m_maxSize.value();
  if (cfg.name.empty())
  {
    ATH_MSG_ERROR("datasetName isn't specified in particle writer");
    return StatusCode::FAILURE;
  }
  for (const std::string &prim : m_primitives)
  {
    if (!m_primToType.value().count(prim))
    {
      ATH_MSG_ERROR(prim << " not specified in type mapping");
      return StatusCode::FAILURE;
    }
    auto type = getPrimitiveType(m_primToType.value().at(prim));
    if (m_primToAssociation.value().count(prim))
    {
      std::string path = m_primToAssociation.value().at(prim);
      size_t pos = path.find('/');
      if (pos == std::string::npos)
      {
        ATH_MSG_ERROR("no '/' in " << path);
        return StatusCode::FAILURE;
      }
      std::string link_name = path.substr(0, pos);
      std::string source_name = path.substr(pos + 1);
      Primitive newprim{type, source_name, prim};
      cfg.inputs.push_back(AssociatedPrimitive{link_name, newprim});
    }
    else
    {
      Primitive newprim{type, prim, prim};
      cfg.inputs.push_back(AssociatedPrimitive{"", newprim});
    }
  }
  m_writer.reset(new PhotonWriter(*m_output_svc->group(), cfg));

  // if(!m_photonPointingTool.empty()){
  //   ATH_CHECK(m_photonPointingTool.retrieve());
  //   ATH_CHECK(m_photonPointingTool->initialize());
  // }

  return StatusCode::SUCCESS;
}

StatusCode PhotonWriterAlg::execute()
{
  const EventContext &ctx = Gaudi::Hive::currentContext();

  SG::ReadHandle<xAOD::VertexContainer> vertexContainer(m_vertexKey);
  ATH_CHECK(vertexContainer.isValid());


  // additional ReadHandleKeys to declare dependencies to the scheduler

  // m_caloPointingZKey = photonBaseName + ".caloPointingZ";
  // m_zCommonKey = photonBaseName + ".zCommon";
  // m_zCommonErrorKey = photonBaseName + ".zCommonError";

  // ATH_CHECK(m_caloPointingZKey.initialize());
  // ATH_CHECK(m_zCommonKey.initialize());
  // ATH_CHECK(m_zCommonErrorKey.initialize());


  const std::string baseName = m_vertexKey.key();
  m_photonLinksKey = baseName + ".photonLinks";
  SG::ReadDecorHandle<xAOD::VertexContainer, std::vector<ElementLink<xAOD::PhotonContainer>>> acc_photonLinks(m_photonLinksKey, ctx);
  ATH_CHECK(acc_photonLinks.isValid());


  SG::AuxElement::Decorator<char> deco_isConv("photonIsConverted");
  SG::AuxElement::Decorator<float> deco_deltaZ("photon_deltaZ");
  SG::AuxElement::Decorator<float> deco_deltaZsigma("photon_deltaZsigma");
  SG::AuxElement::Decorator<float> deco_deltaZsigni("photon_deltaZsigni");
  SG::AuxElement::Decorator<float> deco_deltaZ_bs("photon_deltaZ_wBeamSpot");

  const xAOD::EventInfo *eventInfo = nullptr;
  ANA_CHECK(evtStore()->retrieve(eventInfo, "EventInfo"));

  std::vector<const xAOD::Photon *> parts;



  for (const xAOD::Vertex *vtx : *vertexContainer) 
  {

    if (vtx->vertexType() == xAOD::VxType::NoVtx)
      continue;


    const std::vector<ElementLink<xAOD::PhotonContainer>>& photonLinks = acc_photonLinks(*vtx);

    for (const auto& link : photonLinks) {
      if (!link.isValid()) continue;
      const xAOD::Photon* photon = *link;
      if (!photon) continue;

      static const SG::AuxElement::ConstAccessor<float> acc_f("caloPointingZ");
      float calo = acc_f(*photon);
      deco_deltaZ(*photon) = calo - vtx->z();


      static const SG::AuxElement::ConstAccessor<float> acc_zcommon_first("zCommon");
      float zcommon_first = acc_zcommon_first(*photon);


      static const SG::AuxElement::ConstAccessor<float> acc_zcommon_second("zCommonError");
      float zcommon_second = acc_zcommon_second(*photon);

      deco_deltaZ_bs(*photon) = abs(zcommon_first - vtx->z())/zcommon_second;

      parts.push_back(photon);
    }

    m_writer->fill(parts);
    parts.clear();

  }
  

  return StatusCode::SUCCESS;
}

StatusCode PhotonWriterAlg::finalize()
{
  m_writer->flush();
  return StatusCode::SUCCESS;
}