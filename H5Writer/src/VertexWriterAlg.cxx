#include "src/VertexWriterAlg.h"
#include "H5Writer/VertexWriterConfig.h"

#include "AlgHelpers.h"

#include "H5Cpp.h"

VertexWriterAlg::VertexWriterAlg(const std::string &name, ISvcLocator *loc)
    : AthAlgorithm(name, loc), m_writer(nullptr)
{
}

StatusCode VertexWriterAlg::initialize()
{
  ATH_CHECK(m_vertexInKey.initialize());
    // ATH_CHECK(m_gnnScoreKey.initialize(m_mode == InDet::InDetHardScatterSelectionTool::Mode::HSGN2));


  ATH_CHECK(m_output_svc.retrieve());

  VertexWriterConfig cfg;
  cfg.name = m_dsName.value();
  if (cfg.name.empty())
  {
    ATH_MSG_ERROR("datasetName isn't specified in Vertex writer");
    return StatusCode::FAILURE;
  }

  for (const std::string &prim : m_primitives)
  {
    if (!m_primToType.value().count(prim))
    {
      ATH_MSG_ERROR(prim << " not specified in type mapping");
    }
    std::string type = m_primToType.value().at(prim);
    cfg.inputs.push_back(Primitive{getPrimitiveType(type), prim, prim});
  }
  m_writer.reset(new VertexWriter(*m_output_svc->group(), cfg));

  return StatusCode::SUCCESS;
}

StatusCode VertexWriterAlg::execute()
{
  const EventContext &ctx = Gaudi::Hive::currentContext();
  SG::ReadHandle<xAOD::VertexContainer> vertices(m_vertexInKey, ctx);

  ATH_CHECK(vertices.isValid());



  for (const xAOD::Vertex *vertex : *vertices)
  {
      if (vertex->vertexType() == xAOD::VxType::NoVtx)
        continue;


      m_writer->fill(*vertex);
  }
  return StatusCode::SUCCESS;
}

StatusCode VertexWriterAlg::finalize()
{
  m_writer->flush();
  return StatusCode::SUCCESS;
}
