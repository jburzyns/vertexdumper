#include "src/ElectronWriterAlg.h"
#include "H5Writer/ElectronWriterConfig.h"
#include "xAODTracking/TrackParticle.h"
#include "xAODTruth/xAODTruthHelpers.h"
#include "xAODTruth/TruthParticle.h"
#include "xAODEgamma/ElectronxAODHelpers.h"
#include "AsgDataHandles/ReadDecorHandle.h"


#include "AlgHelpers.h"

#include "H5Cpp.h"

ElectronWriterAlg::ElectronWriterAlg(const std::string &name,
                                       ISvcLocator *loc)
    : AthAlgorithm(name, loc), m_writer(nullptr)
{
}

StatusCode ElectronWriterAlg::initialize()
{
  ATH_CHECK(m_vertexKey.initialize());
  ATH_CHECK(m_electronLinksKey.initialize());
  ATH_CHECK(m_output_svc.retrieve());
  ElectronWriterConfig cfg;
  cfg.name = m_dsName.value();
  cfg.maximum_size = m_maxSize.value();
  if (cfg.name.empty())
  {
    ATH_MSG_ERROR("datasetName isn't specified in particle writer");
    return StatusCode::FAILURE;
  }
  for (const std::string &prim : m_primitives)
  {
    if (!m_primToType.value().count(prim))
    {
      ATH_MSG_ERROR(prim << " not specified in type mapping");
      return StatusCode::FAILURE;
    }
    auto type = getPrimitiveType(m_primToType.value().at(prim));
    if (m_primToAssociation.value().count(prim))
    {
      std::string path = m_primToAssociation.value().at(prim);
      size_t pos = path.find('/');
      if (pos == std::string::npos)
      {
        ATH_MSG_ERROR("no '/' in " << path);
        return StatusCode::FAILURE;
      }
      std::string link_name = path.substr(0, pos);
      std::string source_name = path.substr(pos + 1);
      Primitive newprim{type, source_name, prim};
      cfg.inputs.push_back(AssociatedPrimitive{link_name, newprim});
    }
    else
    {
      Primitive newprim{type, prim, prim};
      cfg.inputs.push_back(AssociatedPrimitive{"", newprim});
    }
  }
  m_writer.reset(new ElectronWriter(*m_output_svc->group(), cfg));

  return StatusCode::SUCCESS;
}

StatusCode ElectronWriterAlg::execute()
{
  const EventContext &ctx = Gaudi::Hive::currentContext();

  SG::ReadHandle<xAOD::VertexContainer> vertexContainer(m_vertexKey);
  ATH_CHECK(vertexContainer.isValid());


  const std::string baseName = m_vertexKey.key();
  m_electronLinksKey = baseName + ".electronLinks";
  SG::ReadDecorHandle<xAOD::VertexContainer, std::vector<ElementLink<xAOD::ElectronContainer>>> acc_electronLinks(m_electronLinksKey, ctx);
  ATH_CHECK(acc_electronLinks.isValid());



  for (const xAOD::Vertex *vertex : *vertexContainer)
  {
    if (vertex->vertexType() == xAOD::VxType::NoVtx)
      continue;
    




    std::vector<const xAOD::Electron *> parts;
    SG::AuxElement::Decorator<float> deco("deltaZ0");


    const std::vector<ElementLink<xAOD::ElectronContainer>>& electronLinks = acc_electronLinks(*vertex);

    for (const auto& link : electronLinks) {
      if (!link.isValid()) continue;
      const xAOD::Electron* electron = *link;
      if (!electron) continue;

      const auto *gsf_trk = electron->trackParticle(0);
      if (!gsf_trk) continue;

      const auto *id_trk = xAOD::EgammaHelpers::getOriginalTrackParticleFromGSF(gsf_trk);
      if (id_trk) {
        deco(*electron) = id_trk->z0() + id_trk->vz() - vertex->z(); 
      } else {
        deco(*electron) = -999;
      }

      parts.push_back(electron);

      // // Check if the electron's track matches one of the vertex tracks
      // bool matchesVertexTrack = false;
      // for (size_t i = 0; i < vertex->nTrackParticles(); i++) {
      //   const xAOD::TrackParticle *trk = vertex->trackParticle(i);
      //   if (trk && id_trk == trk) {
      //     matchesVertexTrack = true;
      //     break;
      //   }
      // }

      // if (matchesVertexTrack) {
      //   parts.push_back(electron);
      // }
    }

    m_writer->fill(parts);
    parts.clear();
  }
  return StatusCode::SUCCESS;
}

StatusCode ElectronWriterAlg::finalize()
{
  m_writer->flush();
  return StatusCode::SUCCESS;
}
