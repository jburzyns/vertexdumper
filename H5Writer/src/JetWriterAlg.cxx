#include "src/JetWriterAlg.h"
#include "H5Writer/JetWriterConfig.h"
#include "xAODTracking/TrackParticle.h"
#include "xAODTruth/xAODTruthHelpers.h"
#include "xAODTruth/TruthParticle.h"
#include "AsgTools/ToolHandle.h"
#include "AsgDataHandles/ReadDecorHandle.h"

#include "AlgHelpers.h"

#include "H5Cpp.h"

JetWriterAlg::JetWriterAlg(const std::string &name,
                                       ISvcLocator *loc)
    : AthAlgorithm(name, loc), m_writer(nullptr)
{
}

StatusCode JetWriterAlg::initialize()
{
  ATH_CHECK(m_vertexKey.initialize());
  ATH_CHECK(m_jetKey.initialize());
  ATH_CHECK(m_jetLinksKey.initialize());

  ATH_CHECK(m_output_svc.retrieve());

  JetWriterConfig cfg;
  cfg.name = m_dsName.value();
  cfg.maximum_size = m_maxSize.value();
  if (cfg.name.empty())
  {
    ATH_MSG_ERROR("datasetName isn't specified in particle writer");
    return StatusCode::FAILURE;
  }
  for (const std::string &prim : m_primitives)
  {
    if (!m_primToType.value().count(prim))
    {
      ATH_MSG_ERROR(prim << " not specified in type mapping");
      return StatusCode::FAILURE;
    }
    auto type = getPrimitiveType(m_primToType.value().at(prim));
    if (m_primToAssociation.value().count(prim))
    {
      std::string path = m_primToAssociation.value().at(prim);
      size_t pos = path.find('/');
      if (pos == std::string::npos)
      {
        ATH_MSG_ERROR("no '/' in " << path);
        return StatusCode::FAILURE;
      }
      std::string link_name = path.substr(0, pos);
      std::string source_name = path.substr(pos + 1);
      Primitive newprim{type, source_name, prim};
      cfg.inputs.push_back(AssociatedPrimitive{link_name, newprim});
    }
    else
    {
      Primitive newprim{type, prim, prim};
      cfg.inputs.push_back(AssociatedPrimitive{"", newprim});
    }
  }
  m_writer.reset(new JetWriter(*m_output_svc->group(), cfg));

  if (!m_trkVtxAssociationTool.empty()){
    ATH_CHECK(m_trkVtxAssociationTool.retrieve());
    ATH_CHECK(m_trkVtxAssociationTool->setProperty("AMVFVerticesDeco","TTVA_AMVFVertices_forReco"));
    ATH_CHECK(m_trkVtxAssociationTool->setProperty("AMVFWeightsDeco","TTVA_AMVFWeights_forReco"));
    ATH_CHECK(m_trkVtxAssociationTool->initialize());
  } 


  return StatusCode::SUCCESS;
}

StatusCode JetWriterAlg::execute()
{

  const EventContext &ctx = Gaudi::Hive::currentContext();

  SG::ReadHandle<xAOD::VertexContainer> vertexContainer(m_vertexKey);
  ATH_CHECK(vertexContainer.isValid());
  SG::ReadHandle<xAOD::JetContainer> jetContainer(m_jetKey);
  ATH_CHECK(jetContainer.isValid());

  const std::string baseName = m_vertexKey.key();
  m_jetLinksKey = baseName + ".jetLinks";
  SG::ReadDecorHandle<xAOD::VertexContainer, std::vector<ElementLink<xAOD::JetContainer>>> acc_jetLinks(m_jetLinksKey, ctx);
  ATH_CHECK(acc_jetLinks.isValid());


  for (const xAOD::Vertex *vertex : *vertexContainer)
  {
    if (vertex->vertexType() == xAOD::VxType::NoVtx)
      continue;



    const std::vector<ElementLink<xAOD::JetContainer>>& jetLinks = acc_jetLinks(*vertex);

    std::vector<const xAOD::Jet*> parts;    // Correct

    SG::AuxElement::Decorator<float> deco("ntracks_ga");

    for (const auto& link : jetLinks) {
      if (!link.isValid()) continue;
      const xAOD::Jet* jet = *link;
      if (!jet) continue;
      std::vector<const xAOD::TrackParticle*> ghostTracks = jet->getAssociatedObjects<xAOD::TrackParticle >(xAOD::JetAttribute::GhostTrack);
      deco(*jet) = ghostTracks.size();
      parts.push_back(jet);

    }
    m_writer->fill(parts);
    parts.clear();
  }

  return StatusCode::SUCCESS;
}

StatusCode JetWriterAlg::finalize()
{
  m_writer->flush();
  if (!m_trkVtxAssociationTool.empty()) ATH_CHECK(m_trkVtxAssociationTool.release());
  return StatusCode::SUCCESS;
}
