#ifndef PHOTON_WRITER_ALG_H
#define PHOTON_WRITER_ALG_H

#include "H5Writer/IH5GroupSvc.h"
#include "H5Writer/PhotonWriter.h"

#include "AthenaBaseComps/AthAlgorithm.h"
#include "GaudiKernel/ServiceHandle.h"
#include "xAODTracking/VertexContainer.h"
#include "xAODEgamma/PhotonContainer.h"
#include "PhotonVertexSelection/PhotonPointingTool.h"

class PhotonWriterAlg : public AthAlgorithm
{
  public:
  PhotonWriterAlg(const std::string &name, ISvcLocator *loc);

  virtual StatusCode initialize() override;
  virtual StatusCode execute() override;
  virtual StatusCode finalize() override;

  private:
  Gaudi::Property<std::vector<std::string>> m_primitives{
      this, "primitives", {}, "List of primatives to print"};
  Gaudi::Property<std::map<std::string, std::string>> m_primToType{
      this, "primitiveToType", {}, "Map from primitive to type"};
  Gaudi::Property<std::map<std::string, std::string>> m_primToAssociation{
      this, "primitiveToAssociation", {}, "Map from primitive to association"};
  Gaudi::Property<std::string> m_dsName{this, "datasetName", "",
                                        "Name of output dataset"};
  Gaudi::Property<unsigned long long> m_maxSize{
      this, "maximumSize", 10, "Maximum number of particles to store"};
  SG::ReadHandleKey<xAOD::VertexContainer> m_vertexKey{
      this, "vertexContainer", "PrimaryVertices", "Vertex container key"};
  SG::ReadDecorHandleKey<xAOD::VertexContainer> m_photonLinksKey{
      this, "photonLinks", "photonLinks", "" };
  ServiceHandle<IH5GroupSvc> m_output_svc{this, "output", "",
                                          "output file service"};

    // additional ReadHandles for the Photons to declare dependencies
    // to the scheduler
    // SG::ReadDecorHandleKey<xAOD::PhotonContainer> m_caloPointingZKey{
    //   this, "caloPointingZKey", "", "" };
    // SG::ReadDecorHandleKey<xAOD::PhotonContainer> m_zCommonKey{
    //   this, "zCommonKey", "", "" };
    // SG::ReadDecorHandleKey<xAOD::PhotonContainer> m_zCommonErrorKey{
    //   this, "zCommonErrorKey", "", "" };


//   ToolHandle<CP::IPhotonPointingTool> m_photonPointingTool {
//       this, "PhotonPointingTool", "CP::IPhotonPointingTool/PhotonPointingTool", "Photon pointing tool to use"};

  std::unique_ptr<PhotonWriter> m_writer;
};

#endif