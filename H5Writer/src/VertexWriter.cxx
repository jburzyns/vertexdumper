#include "H5Writer/VertexWriter.h"
#include "H5Writer/VertexWriterConfig.h"

#include "PrimitiveHelpers.h"

#include "HDF5Utils/Writer.h"
#include "xAODTracking/Vertex.h"

VertexWriter::VertexWriter(H5::Group &group, const VertexWriterConfig &cfg)
{
  Writer_t::consumer_type c;
  for (const auto &input : cfg.inputs)
  {
    detail::addInput(c, input);
  }
  m_writer = std::make_unique<Writer_t>(group, cfg.name, c);
}

VertexWriter::~VertexWriter() = default;

void VertexWriter::fill(const xAOD::Vertex &vertex) { m_writer->fill(vertex); }

void VertexWriter::flush() { m_writer->flush(); }
